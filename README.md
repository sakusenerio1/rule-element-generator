# PF2e Rule Element Generator

An easy way to generate and edit rule elements for the Pathfinder 2e system in Foundry VTT.
To get started, go the an item's Rules tab (you must have Advanced Rule Element UI enabled in System Settings to see the tab), and click the Wand "New Rule Element" or an existing Rule Element's wand icon.
To learn more about rule elements go to https://github.com/foundryvtt/pf2e/wiki/Quickstart-guide-for-rule-elements

### Localizations

Thanks to the Weblate community, PF2e Rule Element Generator has French, Spanish, and Brazilian Portuguese localizations.
If you'd like to provide more translations for this module, visit
https://weblate.foundryvtt-hub.com/engage/rule-element-generator/

### Manual Install

If you are unable to find this module within Foundry VTT's module browser, paste this into the Manifest URL section instead.

`https://gitlab.com/pearcebasmanm/rule-element-generator/-/raw/main/module.json`

